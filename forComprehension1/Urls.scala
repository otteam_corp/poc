import scala.collection.mutable.ArrayBuffer

case class Urls[String](initialElems: String*){
  
  private val elems = ArrayBuffer[String]()

  elems ++= initialElems

  // As map is defined, foreach method is not necessary
  /*
  def foreach(block: String => Unit): Unit = {
    elems.foreach(block)
  }
  */

  def map(f: String => String): Urls[String] = {
    val abMap: ArrayBuffer[String] = elems.map(f)
    Urls(abMap: _*)
  }
}
