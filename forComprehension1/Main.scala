import Urls._

object Main extends App{

  val urls = Urls("firstUrl", "secondUrl", "thirdUrl")

  for {
    i <- urls
  } yield i
}
